package com.example.kenny;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView timetxt;
    TextView scoretx;
    int score;
    ImageView ımageView,ımageView2,ımageView3,ımageView4,ımageView5,ımageView6,ımageView7,ımageView8,ımageView9;
    ImageView[] imageArray;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timetxt=(TextView) findViewById(R.id.txwTime);
        scoretx=(TextView) findViewById(R.id.txwScore);
        score=0;

        ımageView=findViewById(R.id.imageView);
        ımageView2=findViewById(R.id.imageView2);
        ımageView3=findViewById(R.id.imageView3);
        ımageView4=findViewById(R.id.imageView4);
        ımageView5=findViewById(R.id.imageView5);
        ımageView6=findViewById(R.id.imageView6);
        ımageView7=findViewById(R.id.imageView7);
        ımageView8=findViewById(R.id.imageView8);
        ımageView9=findViewById(R.id.imageView9);
        imageArray=new ImageView[]{
                ımageView,ımageView2,ımageView3,ımageView4,ımageView5,ımageView6,ımageView7,ımageView7,ımageView8,ımageView9
        };

        hideImages();

        new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long l) {
                timetxt.setText("Time: "+l/1000);
            }

            @Override
            public void onFinish() {
                timetxt.setText("Time Off");
                handler.removeCallbacks(runnable);
                for(ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);}
                AlertDialog.Builder alert=new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Restart?");
                alert.setMessage("Are you sure to restart game?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //restart
                        Intent intent=getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Game Over", Toast.LENGTH_SHORT).show();
                    }
                });
                alert.show();
            }



        }.start();
    }



    public void score(View view) {

        score++;
        scoretx.setText("Score: "+score);
    }

    public  void hideImages(){
        handler=new Handler();

        runnable=new Runnable() {
            @Override
            public void run() {
                for(ImageView image:imageArray){
                    image.setVisibility(View.INVISIBLE);
                }
                Random rnd=new Random();
                int sayi=rnd.nextInt(9);
                imageArray[sayi].setVisibility(View.VISIBLE);

                handler.postDelayed(this,400);
            }
        };
        handler.post(runnable);

    }
}
